## Endpoints

### Entidade 01

#### Descrição
Retorna informações sobre a entididade 01.

#### URI Padrão
> http://localhost:3000/api/rota01

#### Atributos
| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único do recurso |
| atributo01 | String | Descrição do atributo 01 |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `/api/rota01` | Retorna todos os os recursos Entidade01 |
| GET | `api/rota01/<id>` | Retorna o recurso Entidade01 pelo `id` |
| POST | `api/rota01/` | Insere um novo recurso Entidade01 |
| PUT | `api/rota01/<id>` | Atualiza o recurso Entidade01 pelo `id` |
| DELETE | `api/rota01/<id>` | Deleta o recurso Entidade01 pelo `id` |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=<numero>` | Limita a quantidade de dados de uma requisição |

## Autenticação

Descrição do processo de autenticação