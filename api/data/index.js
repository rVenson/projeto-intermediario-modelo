const mongoose = require('mongoose')
require('dotenv').config()

mongoose.connection.on('connected', function(){
    console.log("Conexão com MongoDB realizada com sucesso!")
})

var options = {
    useNewUrlParser: true,
    useUnifiedTopology: true
}

mongoose.connect(`mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@cluster0-l4dgs.gcp.mongodb.net/unesc?retryWrites=true&w=majority`, options).catch(function(err){
    console.log(err)
})

module.exports = mongoose