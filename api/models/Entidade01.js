const mongoose = require('../data')

const Entidade01Schema = new mongoose.Schema({
    atributo01 : {
        type: String,
        required: true
    }
})

const Entidade01 = mongoose.model('Entidade01', Entidade01Schema)

module.exports = Entidade01