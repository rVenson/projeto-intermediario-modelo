const express = require('express')
const router = express.Router()
const Entidade01 = require('../models/Entidade01')

router.use(express.json())

router.get("/", function(req, res){
    var limit = Number(req.query.limit)
    Entidade01.find().limit(limit).then(function(doc){
        res.status(200).json(doc)
    }).catch(function(err){
        console.error(err)
        res.status(400).json({ "erro": "Erro ao buscar entidades" })
    })
})

router.get("/:id", function(req, res){
    Entidade01.findById(req.params.id, function(err, doc){
        if(err) res.status(400).json({ "erro": "Erro ao buscar entidade" })
        res.status(200).json(doc)
    })
})

router.post("/", function(req, res){

    var entidade = new Entidade01(req.body)

    entidade.save(function(err, doc){
        if(err) res.status(400).json({ "erro": "Erro ao inserir" })
        res.status(200).json(doc)
    })
})

router.put("/:id", function(req, res){

    Entidade01.findByIdAndUpdate(req.params.id, req.body, function(err, doc){
        if(err) res.status(400).json({ "erro": "Erro ao atualizar" })
        res.status(200).json(doc)
    })
})

router.delete("/:id", function(req, res){

    Entidade01.findByIdAndDelete(req.params.id, function(err, doc){
        if(err) res.status(400).json({ "erro": "Erro ao deletar" })
        res.status(200).json(doc)
    })
})

module.exports = router