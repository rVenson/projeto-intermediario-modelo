const express = require('express')
const app = express()
const routes = {
    rota01 : require("./api/routes/rota01")
}
app.use("/rota01", routes.rota01)

app.listen(process.env.port || 3000, function(){
    console.log("Servidor escutando...")
})